import { Document, ObjectId } from 'mongoose';

export interface Instrument extends Document {
  // readonly id: ObjectId;
  readonly name: string;
  readonly category: number;
  readonly url_image: string;
}