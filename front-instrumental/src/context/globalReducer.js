import {
    CREATE_INSTRUMENT,
    DELETE_INTRUMENT,
    FIND_ALL_INSTRUMENTS,
    FIND_ONE_INSTRUMENT,
    UPDATE_INSTRUMENT,
    DELETE_MESSAGE,
    CREATE_MESSAGE,
    SUCCESS_CHANGE
} from "../types";

const globalReducer = (state, action) => {
    switch (action.type) {
        case CREATE_INSTRUMENT:
            return {
                ...state,
                message: `Instrument "${action.payload.name}" created`,
                allInstruments: [...state.allInstruments, action.payload],
            };
        case DELETE_INTRUMENT:
            return {
                ...state,
                message: `Instrument "${action.payload.name}" deleted`,
                allInstruments: [
                    ...state.allInstruments.filter((event) => event._id !== action.payload._id),
                ],
            };
        case FIND_ALL_INSTRUMENTS:
            return {
                ...state,
                allInstruments: action.payload,
            };
        case FIND_ONE_INSTRUMENT:
            return {
                ...state,
                currentInstrument: action.payload,
            };
        case UPDATE_INSTRUMENT:
            return {
                ...state,
                message: "Instrument updated",
                allInstruments: [
                    ...state.allInstruments.map((instrument) => 
                        instrument._id === action.payload._id ? action.payload : instrument
                    ),
                ],
            }
        case CREATE_MESSAGE:
            return {
                ...state,
                message: action.payload,
            };
        case DELETE_MESSAGE:
            return {
                ...state,
                message: null,
            };
        case SUCCESS_CHANGE:
            return {
                ...state,
                success: action.payload,
            };
        default:
            return state;
    }
};
export default globalReducer;