import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import {
    FastifyAdapter,
    NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { AppModule } from './app.module';
// import { InstrumentsModule } from './instruments/instruments.module';

async function bootstrap() {
    // const app = await NestFactory.create<NestFastifyApplication>(
    //   AppModule,
    //   { cors: true },
    //   // new FastifyAdapter()
    // );
    const app = await NestFactory.create(AppModule);
    var whitelist = ['http://localhost:3000', 'http://localhost:4000'];
    app.enableCors({
        origin: function (origin, callback) {
            if (whitelist.indexOf(origin) !== -1) {
                callback(null, true)
            } else {
                callback(null, true)
            }
        },
        allowedHeaders: 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe, undefined',
        methods: "GET,PUT,POST,DELETE,UPDATE,OPTIONS",
        credentials: true,
    });

    const config = new DocumentBuilder()
        .setTitle('Instrument example')
        .setDescription('The instrument API description')
        .setVersion('1.0')
        .addTag('instrument')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    await app.listen(4000);
}
bootstrap();

