import { Inject, Injectable } from '@nestjs/common';
import { CreateInstrumentDto } from './dto/create-instrument.dto';
import { UpdateInstrumentDto } from './dto/update-instrument.dto';
import { Model } from 'mongoose';
import { Instrument } from './interfaces/instrument.interface';

@Injectable()
export class InstrumentService {

  constructor(
    @Inject('INSTRUMENT_MODEL')
    private instrumentModel: Model<Instrument>,
  ){}

  async create(createInstrumentDto: CreateInstrumentDto): Promise<any> {
    const createdInstrument = new this.instrumentModel(createInstrumentDto);
    const respuesta = await createdInstrument.save();
    const respuesta2 = await this.instrumentModel.findOne().sort('-date');
    return respuesta2;
  }
  async findAll():Promise<Instrument[]> {
    const instruments = await this.instrumentModel.find();
    return instruments;
  }

  async findOne(id: string): Promise<any> {
    const instrument = await this.instrumentModel.findById(id);
    return instrument;
  }

  async update(id: string, updateInstrumentDto: UpdateInstrumentDto): Promise<any>  {
    await this.instrumentModel.findByIdAndUpdate(id, updateInstrumentDto);
    return this.findOne(id);
  }

  async remove(id: string): Promise<any> {
    const deleteInstrument = await this.instrumentModel.findByIdAndDelete(id);
    return deleteInstrument;
  }
}
