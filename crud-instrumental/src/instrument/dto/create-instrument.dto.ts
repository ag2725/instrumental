
export class CreateInstrumentDto {
    name: string;
    category: string;
    url_image: string;
}
