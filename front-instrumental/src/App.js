import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import { useContext, useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import globalContext from './context/globalContext';
import { CREATE_INSTRUMENT, DELETE_INTRUMENT, FIND_ALL_INSTRUMENTS, SUCCESS_CHANGE, UPDATE_INSTRUMENT } from "./types";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Toast from 'react-bootstrap/Toast';
import ToastContainer from 'react-bootstrap/ToastContainer';
import { DELETE_MESSAGE } from './types';

function App() {
    const { state, sendServer, sendState } = useContext(globalContext);
    const { allInstruments, message, success } = state;
    const initialInstrument = {
        _id: null,
        name: '',
        category: '',
        url_image: ''
    }
    const [instrument, setInstrument] = useState(initialInstrument)
    const [deleteObject, setdeleteObject] = useState(false)
    const [modalOpen, setModalOpen] = useState(false)
    useEffect(() => {
        sendServer({}, 'GET', FIND_ALL_INSTRUMENTS);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    useEffect(() => {
        if (success) {
            clear();
            sendState(false, SUCCESS_CHANGE)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [success])

    const clear = () => {
        setInstrument(initialInstrument)
        setModalOpen(false)
        setdeleteObject(false)
    }
    const onChange = e => {
        setInstrument({ ...instrument, [e.target.name]: e.target.value })
    }
    const save = (method, type) => {
        sendServer(
            instrument,
            method,
            type
        );
    }
    const loadInstrument = i => {
        clear();
        setInstrument(i);
        setModalOpen(true)
    }
    const clearMessage = () => {
        sendState(null, DELETE_MESSAGE)
    }
    return <Container fluid="lg" className='notranslate' children={
        <>
            <h1 className="text-center" children={`Instruments (${allInstruments.length})`} />
            <table className="table table-striped" children={
                <>
                    <thead children={
                        <tr children={
                            <>
                                <th colSpan={5} className='text-center' children={
                                    <Button onClick={() => { clear(); setModalOpen(true) }} children="New Instrument" />
                                } />
                            </>
                        } />
                    } />
                    <thead children={
                        <tr children={
                            <>
                                <th children="Name" />
                                <th children="Category" />
                                <th children="Image" />
                            </>
                        } />
                    } />
                    <tbody children={
                        allInstruments.map(i =>
                            <tr className='click' key={i._id} onClick={() => loadInstrument(i)} children={
                                <>
                                    <td children={i.name} />
                                    <td children={i.category} />
                                    <td children={<div className='divImage' style={{ backgroundImage: `url('${i.url_image}')` }} />} />
                                </>
                            } />
                        )
                    } />
                    <Modal show={modalOpen} onHide={clear}>
                        <Modal.Header closeButton className={deleteObject ? 'btn-danger' : ''}>
                            <Modal.Title>{deleteObject ? 'CONFIRM DELETION' : (instrument._id ? 'MODIFY' : 'NEW')} INSTRUMENT</Modal.Title>
                        </Modal.Header>
                        <Form onSubmit={e => { e.preventDefault(); save() }}>
                            <Modal.Body className='row'>
                                <Form.Group className="col-lg-6 mb25">
                                    <Form.Label className='bold'>Name</Form.Label>
                                    <Form.Control
                                        autoFocus={Boolean(!instrument.name)}
                                        type="text"
                                        name='name'
                                        onChange={onChange}
                                        value={instrument.name}
                                        required={true}
                                    />
                                </Form.Group>
                                <Form.Group className="col-lg-6 mb25">
                                    <Form.Label className='bold'>Category</Form.Label>
                                    <Form.Control
                                        autoFocus={Boolean(instrument.category)}
                                        type="text"
                                        name='category'
                                        onChange={onChange}
                                        value={instrument.category}
                                        required={true}
                                    />
                                </Form.Group>
                                <Form.Group className="mb-12 mb25">
                                    <Form.Label className='bold'>Url_image</Form.Label>
                                    <Form.Control
                                        autoFocus={Boolean(instrument.url_image)}
                                        type="text"
                                        name='url_image'
                                        onChange={onChange}
                                        value={instrument.url_image}
                                        required={true}
                                    />
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={deleteObject ? () => setdeleteObject(false) : clear}>
                                    CANCELAR
                                </Button>
                                {Boolean(instrument._id) && !deleteObject ? <Button variant="danger" onClick={() => setdeleteObject(true)}>
                                    ELIMINAR
                                </Button> : ''}
                                {deleteObject ? <Button variant="danger" onClick={() => save('DELETE', DELETE_INTRUMENT)}>
                                    CONFIRMAR
                                </Button> : ''}
                                {!deleteObject ? <Button variant="primary" onClick={() => save(instrument._id ? 'PUT' : 'POST', instrument._id ? UPDATE_INSTRUMENT : CREATE_INSTRUMENT)}>
                                    {instrument._id ? 'GUARDAR' : 'CREAR'}
                                </Button> : ''}
                            </Modal.Footer>
                        </Form>
                    </Modal>
                </>
            } />
            {message ?
                <ToastContainer className="p-3 zi1056" position='top-end'>
                    <Toast bg={'warning'} className='noPrint' autohide={true} delay={5000} onClose={clearMessage}>
                        <Toast.Header closeButton={true}>
                            <strong className="me-auto">Info</strong>
                            {/* <small>11 mins ago</small> */}
                        </Toast.Header>
                        <Toast.Body>{message}</Toast.Body>
                    </Toast>
                </ToastContainer>
                : null}
        </>
    } />;
}

export default App;
