import { Module } from '@nestjs/common';
import { InstrumentService } from './instrument.service';
import { InstrumentController } from './instrument.controller';
import { DatabaseModule } from 'src/database/database.module';
import { instrumentProvider } from './instrument.provider';

@Module({
  imports: [ DatabaseModule ],
  controllers: [InstrumentController],
  providers: [InstrumentService, ...instrumentProvider]
})
export class InstrumentModule {}
