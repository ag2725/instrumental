import React, { useReducer } from "react";
import globalContext from "./globalContext";
import globalReducer from "./globalReducer";
import { CREATE_MESSAGE, SUCCESS_CHANGE } from "../types";

function GlobalState(props) {
    const [state, dispatch] = useReducer(globalReducer, {
        allInstruments: [],
        message: null,
        success: false,
    });
    function sendServer(object, method, type) {
        if(method === "GET"){
            fetch(`http://localhost:4000/instrument${object._id?'/'+object._id:''}`).then(
                response => response.json()
            ).then(data => {
                dispatch({
                    type: type,
                    payload: data,
                });
            }).catch(
                error => {
                    if (state.online) {
                        dispatch({
                            type: CREATE_MESSAGE,
                            payload: { message: error },
                        });
                    }
                }
            );
        }else{
            fetch(`http://localhost:4000/instrument${object._id?'/'+object._id:''}`, {
                method: method,
                mode: 'cors',
                body: JSON.stringify(object),
                credentials: "include",
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(
                response => response.json()
            ).then(data => {
                dispatch({
                    type: type,
                    payload: data,
                });
                dispatch({
                    type: SUCCESS_CHANGE,
                    payload: true,
                });
            }).catch(
                error => {
                    if (state.online) {
                        dispatch({
                            type: CREATE_MESSAGE,
                            payload: { message: error },
                        });
                    }
                }
            );
        }
        

    }
    const sendState = (object, type) => {
        dispatch({
            type: type,
            payload: object,
        });
    }
    return (
        <globalContext.Provider
            value={{
                state: state,
                sendServer,
                sendState,
            }}
        >
            {props.children}
        </globalContext.Provider>
    );
}

export default GlobalState;