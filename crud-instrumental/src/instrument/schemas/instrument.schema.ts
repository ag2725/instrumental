import * as mongoose from 'mongoose';

export const InstrumentSchema = new mongoose.Schema({
    // _id: String,
    name: {
        type: String,
        require: true,
    },
    category: String,
    url_image: String,
    date: {
        type: Date,
        default: Date.now
    }
});