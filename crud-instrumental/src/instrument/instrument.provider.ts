import { Connection } from 'mongoose';
import { InstrumentSchema } from './schemas/instrument.schema';

export const instrumentProvider = [
  {
    provide: 'INSTRUMENT_MODEL',
    useFactory: (connection: Connection) => connection.model('Instrument', InstrumentSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];