import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { InstrumentModule } from './instrument/instrument.module';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [InstrumentModule, DatabaseModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
